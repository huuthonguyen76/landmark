import tensorflow as tf
import numpy as np

from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import preprocess_input
from tensorflow.keras import optimizers
from tensorflow.keras.preprocessing.image import ImageDataGenerator

batch_size = 16
datagen = ImageDataGenerator(rescale=1./255)

train_generator = datagen.flow_from_directory(
    './TestVal/',
    target_size=(224, 224),
    batch_size=16,
    class_mode='categorical')

# test_generator = datagen.flow_from_directory(
#     './TestVal/',
#     target_size=(224, 224),
#     batch_size=16,
#     class_mode='categorical')


model = VGG16(weights='imagenet')
for layer in model.layers[:-1]:
    layer.trainable = False

new_model = tf.keras.models.Sequential()
for layer in model.layers[:-1]:
    new_model.add(layer)

new_model.add(tf.keras.layers.Dense(103, activation='softmax', name='prediction'))
new_model.summary()


# Compile the model
new_model.compile(loss='categorical_crossentropy',
              optimizer=optimizers.RMSprop(lr=1e-4),
              metrics=['acc'])

# Train the model
history = new_model.fit_generator(
      train_generator,
      steps_per_epoch=train_generator.samples/train_generator.batch_size ,
      epochs=30,
      validation_data=train_generator,
      validation_steps=train_generator.samples/train_generator.batch_size,
      verbose=1)

# Save the model
new_model.save('small_last4.h5')


