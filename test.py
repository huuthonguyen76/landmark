# import the necessary packages
import numpy as np
import argparse
import cv2
import imutils
import tensorflow as tf

# load the image from disk
image = cv2.imread('/Users/nguyenhuutho/Downloads/TrainVal/0/999.jpg')
print(image.shape)

rotated = cv2.flip(image, 0)
cv2.imshow("", rotated)

rotated = cv2.flip(image, 1)
cv2.imshow("", rotated)

rotated = cv2.flip(image, -1)
cv2.imshow("", rotated)

cv2.waitKey()

# close the windows
cv2.destroyAllWindows()

# # loop over the rotation angles
# for angle in np.arange(0, 360, 15):
# 	rotated = imutils.rotate(image, angle)
# 	cv2.imshow("Rotated (Problematic)", rotated)
# 	cv2.waitKey(0)

# loop over the rotation angles again, this time ensuring
# no part of the image is cut off
for angle in np.arange(0, 260, 15):
	rotated = imutils.rotate_bound(image, angle)
	cv2.imshow("Rotated (Correct)", rotated)
	cv2.waitKey(0)


