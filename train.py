import tensorflow as tf
import numpy as np

from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import preprocess_input


model = VGG16(weights='imagenet')
for layer in model.layers[:-1]:
    layer.trainable = False

new_model = tf.keras.models.Sequential()
for layer in model.layers[:-1]:
    new_model.add(layer)

new_model.add(tf.keras.layers.Dense(103, activation='softmax', name='prediction'))
new_model.summary()

for layer in new_model.layers:
    print layer, layer.trainable

